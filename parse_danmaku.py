"""This file defines a class called Danmaku, and a few methods to parse it"""
import datetime


class Danmaku:
    """Danmaku
    
    attributes:
        Danmaku.content, str, the content of danmaku;
        Danmaku.attr, str, the attributes passed in;
        Danmaku.time, datetime.timedelta, the timedelta to the beginning of the video part;
        Danmaku.type, int, the type of the danmaku;
        Danmaku.font_size, int, the font size of danmaku;
        Danmaku.font_color, str(hex), the RGB value of the color of the danmaku;
        Danmaku.datetime, datetime.datetime, the datetime of the danmaku published;
        Danmaku.pool, int, the id of the pool;
        Danmaku.user_hash, str(usually lower hex, but sometimes upper hex), the unique hash value of user;
        Danmaku.id, int, the id of the danmaku in the database

    methods:
        pass
    """
    def __init__(self, attr, content):
        self.content = content
        self.attr = attr
        self.time = None
        self.type = None
        self.font_size = None
        self.font_color = None
        self.datetime = None
        self.pool = None
        self.user_hash = None
        self.id = None

    def __repr__(self):
        return "{}: {!r}".format(str(self.time).split('.')[0], self.content)

    def parse_danmaku(self):
        s = self.attr
        if not isinstance(s, str):
            raise TypeError('str is needed, got {!r}'.format(s))
        attr = s.split(',')
        if len(attr) != 8:
            raise ValueError('danmaku string {!r} not valid'.format(s))
        self.time = datetime.timedelta(seconds=float(attr[0]))
        self.type = int(attr[1])
        self.font_size = int(attr[2])
        self.font_color = hex(int(attr[3]))
        self.datetime = datetime.datetime.fromtimestamp(int(attr[4]))
        self.pool = int(attr[5])
        self.user_hash = attr[6]
        self.id = int(attr[7])

    def to_db(self):
        pass  # todo

d = Danmaku("151.84300231934,1,25,16777215,1402294879,0,Ddb04e08,3271964994", "无与伦比的古典美")
d.parse_danmaku()
