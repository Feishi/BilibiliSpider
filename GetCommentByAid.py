import requests
import re
# from bs4 import BeautifulSoup

v_url_temp = "https://www.bilibili.com/video/av{av_id}/"  # video url template
c_url_temp = "https://comment.bilibili.com/{c_id}.xml"  # comment url template
ua = "Mozilla/5.0 (Windows NT 7.0; Win32; x86) AppleWebKit/537.36 (KHTML, l" \
     "ike Gecko) Chrome/59.0.3051.115 Safari/537.36"


class BVideo:
    def __init__(self, aid):
        self.aid = aid
        self.is_multi = None
        self.cid = []
        self.session = requests.Session()
        self.session.headers.update({'User-Agent': ua})

    def get_info(self):
        s = self.session
        v_url = v_url_temp.format(av_id=self.aid)
        r = s.get(v_url)
        play_list = re.findall(r"cid='(\d+)'", r.text)
        if play_list:  # multi-part video
            self.is_multi = True
            self.cid = play_list
        else:  # single part video
            self.is_multi = False
            self.cid.append(re.search(r"cid=(\d+)&aid", r.text).group(1))
        # print(self.cid)

    def get_current_danmaku(self):
        pass  # todo

    def get_all_danmaku(self):
        pass  # todo


def timeit(f):
    from time import time
    t0 = time()
    f()
    print(time() - t0)
    return f


@timeit
def fun():
    b = BVideo('120040')
    b.get_info()
