# Scrape data from bilibili.com

## 1. Scrape data

## 2. Handle data

### For every video(with distinct av id)
- This video may contain many part(even if every part is one video file)

### For every file
- Every part of one video has one corresponding xml danmaku file
- This file didn't contain all the danmaku of the part of video, only the recent *limit*(marked with <maxlimit> label) number of danmakus will be remain in this file.
- Every danmaku is a <d> label

### For every danmaku
- The content(text) of danmakus lie in the string of <d> label
- the extra information of danmaku define in the attribute 'p'.

### For every 'p' attribute
- The string of 'p' attribute contains 8 attributes of danmaku separated with ','
- Attr 1: *float* like, the seconds of this danmaku beginning from the start of the part of video
- Attr 2: *int* like, the kind of the danmaku, often this can be from 1 to 5
- Attr 3: *int* like, the font size of the danmaku
- Attr 4: *int* like, the font color of the danmaku
- Attr 5: *int* like, the unix time stamp of the danmaku
- Attr 6: *int* like, the danmaku pool
- Attr 7: *hex int* like, the hash of ID of user who send it
- Attr 8: *int* like, the id of the danmaku in the history danmaku database